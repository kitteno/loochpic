# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from contents import models

class ProjectSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return models.Project.objects.all()

    def location(self, obj):
        return "/project_details/%s/" % obj.id

class ServiceSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return models.Service.objects.all()

    def location(self, obj):
        return "/service_details/%s/" % obj.id

class MainSitemap(Sitemap):
    changefreq = "monthly"
    priority = 1

    def items(self):
        return ['index', 'services', 'projects']

    def location(self, item):
        return reverse(item)

class OtherSitemap(Sitemap):
    changefreq = "never"
    priority = 0.3

    def items(self):
        return ['contact']

    def location(self, item):
        return reverse(item)