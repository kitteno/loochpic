# -*- coding: utf-8 -*-

from django.db import models
from sorl.thumbnail import ImageField, get_thumbnail
from _mysql import NULL
# from django.contrib.sites.models import Site


ORIGIN = "orig"
SPECIAL = "spec"
SITE_PART_CHOICES = (
    (ORIGIN, u'Обыкновенный'),
    (SPECIAL, u'Специальный'),
)


# Create your models here.

class Project(models.Model):
    description = models.TextField(u'Полное описание')
    date = models.DateField(u'Дата проекта')
    title = models.CharField(u'Название', max_length=100)
    short_description = models.TextField(u'Краткое описание')
    cover_image = ImageField(u'Обложка', upload_to='projects/cover', help_text=u'Размер 281x281 пикселей.')
    html_title = models.CharField(u'Атрибут "Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    page_description = models.TextField(u'Мета тег "description"')
    page_keywords = models.TextField(u'Мета тег "keywords"')

    site_part = models.CharField(u'Тип проекта', max_length=4, choices=SITE_PART_CHOICES, default=ORIGIN)

    class Meta:
        ordering = ['-date']
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'

    def __unicode__(self):
        return self.title

class Image(models.Model):
    project = models.ForeignKey(Project)
    image = ImageField(u'Фотография', upload_to='projects/images', help_text=u'Размер 940*529 пикселей.')
    html_title = models.CharField(u'Атрибут "Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    ordering = models.PositiveIntegerField(u'Порядок вывода', default=0, db_index=True)

    class Meta:
        ordering = ('ordering',)
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return self.image.path

class ProjectVideo(models.Model):
    project = models.ForeignKey(Project)
    link = models.URLField(u'Адрес Vimeo')
    video_image = ImageField(u'Обложка для видео', upload_to='projects/video/images', help_text=u'Размер 940*529 пикселей.')
    vimeo_number = models.CharField(u'Номер Vimeo', max_length=50)
    image_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    image_title = models.CharField(u'"Title"', max_length=100)

    class Meta:
        verbose_name = u'Видео'
        verbose_name_plural = u'Все видео'

    def __unicode__(self):
        return self.link

class Service(models.Model):
    title = models.CharField(u'Название', max_length=100)
    short_description = models.TextField (u'Описание')
    ordering = models.PositiveIntegerField(u'Порядок вывода', default=0, db_index=True)
    image = ImageField(u'Обложка для услуги', upload_to='services/images', help_text=u'Размер 940*529 пикселей.')
    image_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    image_title = models.CharField(u'Атрибут "Title"', max_length=100)
    main_page = models.BooleanField(u'Показывать на главной странице', default=False, db_index=True)
    veryshort_description = models.TextField (u'Краткое описание', blank=True)
    page_description = models.TextField(u'Мета тег "description"')
    page_keywords = models.TextField(u'Мета тег "keywords"')
    page_h1 = models.CharField(u'Заголовок H1' , blank=True, max_length=600)
    page_h1.default = ''
    
    page_alias = models.CharField(u'Алиас', blank=True, max_length=255)
    page_alias.default = ''
    
    page_title = models.CharField(u'Тайтл', blank=True, max_length=255)
    page_title.default = ''

    site_part = models.CharField(u'Тип услуги', max_length=4, choices=SITE_PART_CHOICES, default=ORIGIN)
    special_service = models.BooleanField(u'Спец услуга', default=False, db_index=True)

    class Meta:
        ordering = ['ordering']
        verbose_name = u'Услуга'
        verbose_name_plural = u'Услуги'

    def __unicode__(self):
        return self.title

class ServiceVideo(models.Model):
    service = models.ForeignKey(Service)
    link = models.URLField(u'Адрес Vimeo')
    video_image = ImageField(u'Обложка для видео', upload_to='services/video/images', help_text=u'Размер 940*529 пикселей.')
    vimeo_number = models.CharField(u'Номер Vimeo', max_length=50)
    image_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    image_title = models.CharField(u'"Title"', max_length=100)

    class Meta:
        verbose_name = u'Видео'
        verbose_name_plural = u'Все видео'

    def __unicode__(self):
        return self.link

class SubService(models.Model):
    service = models.ForeignKey(Service)
    title = models.CharField(u'Наименование', max_length=100)
    descritpion = models.TextField(u'Описание')
    image = ImageField(u'Обложка', upload_to='subservices/images', help_text=u'Размер 281x281 пикселей.')
    image_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    image_title = models.CharField(u'Атрибут "Title"', max_length=100)

    class Meta:
        verbose_name = u'Подпункт услуги'
        verbose_name_plural = u'Подпункты услуги'

    def __unicode__(self):
        return self.title

class SubImage(models.Model):
    service = models.ForeignKey(SubService)
    image = ImageField(u'Фотография', upload_to='subservices/images', help_text=u'Размер 281x281 пикселей.')
    is_cover_image = models.BooleanField(u'Обложка?', default=False, db_index=True)
    html_title = models.CharField(u'Атрибут "Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)

    class Meta:
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return self.image.path

    def cover_image(self):
        try:
            return self.image_set.get(is_cover_image=True)
        except SubImage.DoesNotExist:
            return None

    def not_cover_image(self):
        try:
            return self.image_set.filter(is_cover_image=False)
        except SubImage.DoesNotExist:
            return None

class Contact(models.Model):
    title = models.CharField(u'Наименование', max_length=100)
    adress = models.CharField(u'Адрес', max_length=100)
    phone = models.CharField(u'Телефон', max_length=100)
    fax = models.CharField(u'Факс', max_length=100)
    email = models.CharField(u'Email', max_length=100)
    gl_country = models.CharField(u'Страна', max_length=50)
    gl_city = models.CharField(u'Город', max_length=50)
    gl_street = models.CharField(u'Улица', max_length=50)
    gl_bld = models.CharField(u'Дом', max_length=5)
    gl_korpus = models.CharField(u'Корпус', max_length=5, blank=True)

    class Meta:
        verbose_name = u'Контакты'
        verbose_name_plural = u'Контакты'

    def __unicode__(self):
        return self.title

class CompanyInfo(models.Model):
    title = models.CharField(u'Наименование', max_length=100)
    description = models.TextField (u'Описание')
    ordering = models.PositiveIntegerField(u'Порядок вывода', default=0, db_index=True)

    site_part = models.CharField(u'Тип информации', max_length=4, choices=SITE_PART_CHOICES, default=ORIGIN)

    class Meta:
        ordering = ['ordering']
        verbose_name = u'Информация о компании'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.title

class MainTeaser(models.Model):
    title = models.CharField(u'Наименование', max_length=100)
    image = ImageField(u'Фото', upload_to='mainteaser/images', help_text=u'Размер 1920x550 пикселей.')
    ordering = models.PositiveIntegerField(u'Порядок вывода', default=0, db_index=True)
    html_title = models.CharField(u'Атрибут "Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)

    class Meta:
        ordering = ['ordering']
        verbose_name = u'Тизер на главной странице'
        verbose_name_plural = u'Тизеры на главной странице'

    def __unicode__(self):
        return self.title

class SubTeaser(models.Model):
    title = models.CharField(u'Наименование', max_length=100)
    image = ImageField(u'Фото', upload_to='subteaser/images', help_text=u'Размер 1920x550 пикселей.')
    html_title = models.CharField(u'Атрибут "Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)

    class Meta:
        verbose_name = u'Тизер на дополнительных страницах'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.title

class ServiceImage(models.Model):
    service = models.ForeignKey(Service)
    image = ImageField(u'Фотография', upload_to='services/images', help_text=u'Размер 940*529 пикселей.')
    html_title = models.CharField(u'"Title"', max_length=100)
    html_alt = models.CharField(u'Атрибут "Alt"', max_length=100)
    ordering = models.PositiveIntegerField(u'Порядок вывода', default=0, db_index=True)

    class Meta:
        ordering = ('ordering',)
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return self.image.path

class HtmlAttributes(models.Model):
    MAIN = "mn"
    SERVICES = "sr"
    PROJECTS = "pr"
    CONTACT = "ct"
    PAGE_TYPE_CHOICES = (
        (MAIN, u'Главная'),
        (SERVICES, u'Сервисы'),
        (PROJECTS, u'Проекты'),
        (CONTACT, u'Контакты'),
    )
    page_type = models.CharField(max_length=2, choices=PAGE_TYPE_CHOICES, default=MAIN)
    page_title = models.CharField(u'Атрибут "Title" страницы', max_length=100)
    page_description = models.TextField(u'Мета тег "description"')
    page_keywords = models.TextField(u'Мета тег "keywords"')

    class Meta:
        verbose_name = u'HTML атрибут для основных страниц'
        verbose_name_plural = u'HTML атрибуты для основных страниц'

    def __unicode__(self):
        return self.page_title