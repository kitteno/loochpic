# -*- coding: utf-8 -*-

from django.contrib import admin
from contents import models
from sorl.thumbnail.admin import AdminImageMixin, AdminInlineImageMixin


class ImageInline(AdminInlineImageMixin, admin.TabularInline):
    model = models.Image


class VideoInline(AdminInlineImageMixin, admin.StackedInline):
    fieldsets = [
        ( None, {'fields': ['video_image', ('link', 'vimeo_number'), ('image_alt', 'image_title')]})
    ]
    model = models.ProjectVideo


class ProjectAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('title', 'date')
    fieldsets = [
        (u'Текст',
         {'fields': ['title', 'short_description', 'description', 'site_part', ('page_description', 'page_keywords')]}),
        ( None, {'fields': [('cover_image', 'html_title', 'html_alt')]}),
        ( None, {'fields': ['date']}),
    ]
    inlines = [ImageInline, VideoInline]


class ServiceVideoInline(AdminInlineImageMixin, admin.StackedInline):
    fieldsets = [
        ( None, {'fields': ['video_image', ('link', 'vimeo_number'), ('image_alt', 'image_title')]})
    ]
    model = models.ServiceVideo


class ServiceImageInline(AdminInlineImageMixin, admin.StackedInline):
    fieldsets = [
        ( None, {'fields': [('image', 'ordering'), ('html_alt', 'html_title')]})
    ]
    model = models.ServiceImage


class ServiceAdmin(AdminInlineImageMixin, admin.ModelAdmin):
    fields = (
        ('title', 'main_page', 'special_service'), 'veryshort_description', 'short_description', 'ordering', 'site_part',
        ('image', 'image_alt', 'image_title'), ('page_description', 'page_keywords'),
        ('page_h1', 'page_alias', 'page_title'))
    inlines = [ServiceVideoInline, ServiceImageInline]


class SubServiceImageInLine(AdminInlineImageMixin, admin.TabularInline):
    model = models.SubImage


class SubServiceAdmin(AdminImageMixin, admin.ModelAdmin):
    pass


class ContactAdmin(admin.ModelAdmin):
    fieldsets = [
        ( u'Текст', {'fields': ['title', 'adress', 'phone', 'fax', 'email']}),
        ( u'Данные для google map', {'fields': ['gl_country', 'gl_city', 'gl_street', 'gl_bld', 'gl_korpus']}),
    ]

    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True


class CompanyInfoAdmin(admin.ModelAdmin):
    pass


class MainTeaserAdmin(AdminImageMixin, admin.ModelAdmin):
    pass


class SubTeaserAdmin(AdminImageMixin, admin.ModelAdmin):
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True


class HtmlAttributesAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.Service, ServiceAdmin)
admin.site.register(models.SubService, SubServiceAdmin)
admin.site.register(models.Contact, ContactAdmin)
admin.site.register(models.CompanyInfo, CompanyInfoAdmin)
admin.site.register(models.MainTeaser, MainTeaserAdmin)
admin.site.register(models.SubTeaser, SubTeaserAdmin)
admin.site.register(models.HtmlAttributes, HtmlAttributesAdmin)